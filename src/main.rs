extern crate core_affinity;

use rand::Rng;
use std::{thread, time};

fn run_test(mem: &Vec<usize>) -> usize {
    let mut value: usize = mem[0];
    for _i in 0..mem.len() {
        value = mem[value];
    }
    return value;
}

fn main() {
    println!("Linux CPU Max Boost Tester");
    const HALF_ARRAY: usize = 0x1FFFFFF + 1;
    println!("HALF_ARRAY: {}", HALF_ARRAY);

    const ARRAY_SIZE: usize = HALF_ARRAY * 2;
    println!("ARRAY_SIZE: {}", ARRAY_SIZE);

    let memsize = ARRAY_SIZE / 256 / 1024;
    println!("Memory required: {} MB", memsize);

    let mut mem: Vec<usize> = Vec::new();

    println!("Filling memory array");

    for number in (0..ARRAY_SIZE).rev() {
        if number < HALF_ARRAY {
            mem.push(number + HALF_ARRAY);
        } else {
            mem.push(number - HALF_ARRAY);
        }
    }

    println!("Performing array shuffle (low)");

    let mut rng = rand::thread_rng();

    for x in (0..(HALF_ARRAY - 1)).rev() {
        let r: usize = rng.gen::<usize>() % HALF_ARRAY;
        let temp: usize = mem[x];
        mem[x] = mem[r];
        mem[r] = temp;
    }

    println!("Performing array shuffle (high)");
    for x in (HALF_ARRAY..ARRAY_SIZE).rev() {
        let r: usize = (rng.gen::<usize>() % HALF_ARRAY) + HALF_ARRAY;
        let temp: usize = mem[x];
        mem[x] = mem[r];
        mem[r] = temp;
    }

    // Retrieve the IDs of all active CPU cores.
    let core_ids = core_affinity::get_core_ids().unwrap();
    println!("Found core #{:?}", core_ids);

    let mut counter: usize = 0;
    let timeout = time::Duration::from_millis(100);
    loop {
        for id in (&core_ids).into_iter() {
            println!("Setting affinity to core #{:?}", id);
            core_affinity::set_for_current(*id);
            thread::sleep(timeout);
            counter = run_test(&mem);
        }
        println!("Done. Counter: {}", counter);
    }
}
